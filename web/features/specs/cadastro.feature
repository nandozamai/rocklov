#language: pt

Funcionalidade: Cadastro
    Sendo um músico que possui equipamentos musicais
    Quero fazer o meu cadastro no RockLov
    Para que eu possa disponibilizá-los para locação

@cadastro
Cenario: Fazer cadastro

    Dado que acesso a página de cadastro
    Quando submeto o seguinte formulário de cadastro:
        |nome          |email             |senha |
        |Fernando Filho|fernando@gmail.com|pwd123|
    Então sou redirecionado para o Dashboard

@tentativa_cadastro
Cenario: Submeter cadastro sem o nome

    Dado que acesso a página de cadastro
    Quando submeto o seguinte formulário de cadastro:
        |nome          |email             |senha |
        |              |fernando@gmail.com|pwd123|
    Então vejo a mensagem de alerta: "Oops. Informe seu nome completo!"

Esquema do Cenario: Tentativa de Cadastro
    Dado que acesso a página de cadastro
    Quando submeto o seguinte formulário de cadastro:
        |nome        |email        |senha        |
        |<nome_input>|<email_input>|<senha_input>|
    Então vejo a mensagem de alerta: "<mensagem_output>"

    Exemplos:
    |nome_input    |email_input       |senha_input|mensagem_output                 |
    |              |fernando@gmail.com|pwd123     |Oops. Informe seu nome completo!|
    |Fernando Filho|                  |pwd123     |Oops. Informe um email válido!  |
    |Fernando Filho|fernando*gmail.com|pwd123     |Oops. Informe um email válido!  |
    |Fernando Filho|fernando@gmail.com|           |Oops. Informe sua senha secreta!|
