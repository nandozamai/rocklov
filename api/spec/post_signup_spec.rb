require_relative "routes/signup"
require_relative "libs/mongo"

describe "POST / signup" do
  context "novo usuario" do
    before(:all) do
      payload = { name: "Pitty", email: "pitty@bol.com.br", password: "pwd123" }
      MongoDB.new.remove_user(payload[:email])

      @result = Signup.new.create(payload)
    end

    it "valida status code" do
      expect(@result.code).to eql 200
    end

    it "valida id do usuario" do
      #parsed_response converte para um hash do Ruby
      expect(@result.parsed_response["_id"].length).to eql 24
    end
  end

  context "usuario ja existe" do
    before(:all) do
      # Dado que eu tenho um novo usuario
      payload = { name: "Joao da Silva", email: "joao@ig.com.br", password: "pwd123" }
      MongoDB.new.remove_user(payload[:email])

      # E o email desse usuario ja foi cadastrado no sistema
      Signup.new.create(payload)

      #Quando faço uma requisição para a rota /signup
      @result = Signup.new.create(payload)
    end

    it "deve retornar 409" do
      # Então deve retornar 409
      expect(@result.code).to eql 409
    end

    it "deve retornar mensagem" do
      #parsed_response converte para um hash do Ruby
      expect(@result.parsed_response["error"]).to eql "Email already exists :("
    end
  end

  # examples = [
  #   {
  #     title: "nome em branco",
  #     payload: { name: "", email: "joao@ig.com.br", password: "pwd123" },
  #     code: 412,
  #     error: "required name",
  #   },
  #   {
  #     title: "email em branco",
  #     payload: { name: "Joao da Silva", email: "", password: "pwd123" },
  #     code: 412,
  #     error: "required email",
  #   },
  #   {
  #     title: "senha em branco",
  #     payload: { name: "Joao da Silva", email: "joao@ig.com.br", password: "" },
  #     code: 412,
  #     error: "required password",
  #   },
  # ]

  puts examples.to_json
  examples = Helpers::get_fixture("signup")

  examples.each do |e|
    context "#{e[:title]}" do
      before(:all) do
        @result = Signup.new.create(e[:payload])
      end

      it "valida status code #{e[:code]}" do
        expect(@result.code).to eql e[:code]
      end

      it "valida mensagem de erro" do
        #parsed_response converte para um hash do Ruby
        expect(@result.parsed_response["error"]).to eql e[:error]
      end
    end
  end
end
